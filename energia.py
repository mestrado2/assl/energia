# -*- coding: utf-8 -*-
"""
Elliott Victor de Sousa Chaves
Mat: 20201630015
Disciplina: Análise de Sinais e Sistemas

"""
import matplotlib.pyplot as plot
import numpy

from scipy.io import wavfile

samplingFrequency, signalData = wavfile.read('./amostras/xc443364.wav')

max_value = numpy.amax(signalData)

newSignal = numpy.empty([1])
energia = numpy.empty([1])

for x in range(0,len(signalData)):
    newSignal = numpy.append(newSignal,[(signalData[x]/max_value)])
    
energia = sum((abs(newSignal)**2))

print("Energia: ", energia)

plot.subplot(211)

plot.title('Myiodynastes maculatus - Cerrado')

eixo_x = numpy.linspace(0, len(newSignal) / samplingFrequency, num=len(newSignal)) 

plot.plot(eixo_x,newSignal)

plot.xlabel('Tempo')

plot.ylabel('Amplitude')
